Concepts of OOPS:
1. Abstraction
2. Encapsulation
3. Inheritance
4. Polymorphisum

Refer:
https://beginnersbook.com/2013/04/oops-concepts/

What is Abstraction?
Hiding of data from external sources, by providing access specifiers like public, private, protected.

Refer: 
https://www.journaldev.com/33191/what-is-abstraction-in-oops

What is Encapsulation?
Wrapping of Data and memeber functions is called Abstraction, Example objects and classes.

Refer: 
https://www.geeksforgeeks.org/encapsulation-in-java/

What is Inheritance?
Ability to inherit properties and mechanisum of a different class is called Inheritance.
1. Single Inheritance
2. Multi level Inheritance
3. Hierarchial Inheritance
Note: Java doesn't support multi level inheritance, we can do Multiple inheritance by interface classes

Refer:
https://www.javatpoint.com/inheritance-in-java

We will talk about extends keyword and implements keyword, we will also talk about abstract classes and interfaces

What is polymorphisum?
Ability to reuse a code is called polymorphisum.
1. Compile time polymorphisum:
	Example: Overloading 
2. Run time polymorphisum:
	Example: Overriding
	
Refer: 
https://www.tutorialspoint.com/java/java_polymorphism.htm
