package OOPS;

import SubOoops.SubAbstraction2;

/*What is Abstraction?
* Abstraction is about hiding unwanted details while showing most essential information.
* Example : private, public, protected, default*/
public class Abstraction extends SubAbstraction2 {
    public static void main(String[] args) {
        SubAbstraction subAbstraction = new SubAbstraction();

        SubAbstraction2 subAbstraction2 = new SubAbstraction2();

        /*public metods */
        System.out.println("Sum = "+subAbstraction.sum(1,2));
        double result = subAbstraction.sum(1,4.0);
        System.out.println("Sum = "+result);
        subAbstraction.sumNoReturn(1,4.0);

        /*protected methods */
        System.out.println("Sum = "+subAbstraction.sumIntegerProtected(1,2));

        System.out.println(SubAbstraction2.fullName);
        System.out.println(SubAbstraction2.fullName2);
    }
}

/*Class level : Private is not allowed, Protected is not allowed
*               : only public and default are allowed */

/* Method Level: private methods can be accessed only in delcared class cannot be used outside, i.e scope is with in class only
*                 public methods can be accessed by declaring an object for the class, i.e scope is anywhere with instance of an object
*                 protected method is same as private*/

/* Variable Level: private: Scope is only untill class level only, cannot be accessed outside class
                    public: can be accessed from anywhere with class instance
                    protected: If the class is being extended or implemented i.e inheriting properties of the other class you can access protected variables in that class
                    static: Creates a class level instance for the variable, could only accessed by <classname.variable name>
 */

