package OOPS;
/* What is Inheritance?
* Ability inherit properties of another class is called inheritance */
public class Inheriatnce {
    public static void main(String[] args) {
        Dog dog = new Dog("dolmassion","street dog", "crnivorous","dolmassion", "Misty");
        System.out.println("Breed: "+dog.getBreed()+" Type :"+dog.getTypeOfAnimal()+" Food :"+dog.getFoodType()+
                " dogBreed : "+dog.getDogBreed()+" Name: "+dog.getName());

        Dog dog1 = new Dog("Huskey","stray dog", "herbivorous","Huskey", "Fox");
        System.out.println("Breed: "+dog1.getBreed()+" Type :"+dog1.getTypeOfAnimal()+" Food :"+dog1.getFoodType()+
                " dogBreed : "+dog1.getDogBreed()+" Name: "+dog1.getName());

    }
}

class Animal{
    private String breed;
    private String typeOfAnimal;
    private String foodType;

    public Animal(String breed, String typeOfAnimal, String foodType) {
        this.breed = breed;
        this.typeOfAnimal = typeOfAnimal;
        this.foodType = foodType;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public void setTypeOfAnimal(String typeOfAnimal) {
        this.typeOfAnimal = typeOfAnimal;
    }

    public void setFoodType(String foodType) {
        this.foodType = foodType;
    }

    public String getBreed() {
        return breed;
    }

    public String getTypeOfAnimal() {
        return typeOfAnimal;
    }

    public String getFoodType() {
        return foodType;
    }
}

class Dog extends Animal{

    private String DogBreed;
    private String name;
    public Dog(String breed, String typeOfAnimal, String foodType, String dogBreed, String name) {
        super(breed, typeOfAnimal, foodType);
        this.DogBreed = dogBreed;
        this.name = name;
    }

    public String getDogBreed() {
        return DogBreed;
    }

    public String getName() {
        return name;
    }

}

class Lion extends Animal{
    private String lionBreed;
    private String name;

    public Lion(String breed, String typeOfAnimal, String foodType, String lionBreed, String name) {
        super(breed, typeOfAnimal, foodType);
        this.lionBreed = lionBreed;
        this.name = name;
    }

    public String getLionBreed() {
        return lionBreed;
    }

    public String getName() {
        return name;
    }
}

class Fish extends Animal{
    private String fishBreed;
    private String name;

    public Fish(String breed, String typeOfAnimal, String foodType, String fishBreed, String name) {
        super(breed, typeOfAnimal, foodType);
        this.fishBreed = fishBreed;
        this.name = name;
    }

    public String getFishBreed() {
        return fishBreed;
    }

    public String getName() {
        return name;
    }
}