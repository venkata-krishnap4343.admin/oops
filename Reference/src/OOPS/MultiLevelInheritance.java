package OOPS;

public class MultiLevelInheritance {
    public static void main(String[] args) {
        BankAccount bankAccount = new BankAccount("SBI", "National", "Ashok Nagar", "Ashok Nagar 520007", "100", "Sai Krishna");
        System.out.println(" Bank Name"+bankAccount.getBankName());
        System.out.println(" Type of Bank"+bankAccount.getBankType());
        System.out.println(" Branch"+bankAccount.getBranchName());
        System.out.println(" Address"+bankAccount.getAddress());
        System.out.println(" Account Number"+bankAccount.getAccountNumber());
        System.out.println(" Account Name"+bankAccount.getAccountName());
        System.out.println("*************************************");
        bankAccount.setBankName("ICICI");
        bankAccount.setBankType("Private");
        System.out.println(" Bank Name"+bankAccount.getBankName());
        System.out.println(" Type of Bank"+bankAccount.getBankType());
        System.out.println(" Branch"+bankAccount.getBranchName());
        System.out.println(" Address"+bankAccount.getAddress());
        System.out.println(" Account Number"+bankAccount.getAccountNumber());
        System.out.println(" Account Name"+bankAccount.getAccountName());



        BankAccount bankAccount2 = new BankAccount("SBI", "National", "Ashok Nagar", "Ashok Nagar 520007", "100", "Sai Krishna");

        System.out.println("*************************************");
        bankAccount.setBankName("HSBC");
        bankAccount.setBankType("Private");
        System.out.println(" Bank Name"+bankAccount2.getBankName());
        System.out.println(" Type of Bank"+bankAccount2.getBankType());
        System.out.println(" Branch"+bankAccount2.getBranchName());
        System.out.println(" Address"+bankAccount2.getAddress());
        System.out.println(" Account Number"+bankAccount2.getAccountNumber());
        System.out.println(" Account Name"+bankAccount2.getAccountName());

        System.out.println("*************************************");

        BankBranch bankBranch = new BankBranch("Bank of Baroda", "Private", "Patamata","520010");
        System.out.println(" Bank Name"+bankBranch.getBankName());
        System.out.println(" Type of Bank"+bankBranch.getBankType());
        System.out.println(" Branch"+bankBranch.getBranchName());
        System.out.println(" Address"+bankBranch.getAddress());

    }
}

class Bank {
    private String bankName;
    private String bankType;

    public Bank(String bankName, String bankType) {
        this.bankName = bankName;
        this.bankType = bankType;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public void setBankType(String bankType) {
        this.bankType = bankType;
    }

    public String getBankName() {
        return bankName;
    }

    public String getBankType() {
        return bankType;
    }
}

class BankBranch extends Bank{
    private String branchName;
    private String address;

    public BankBranch(String bankName, String bankType, String branchName, String address) {
        super(bankName, bankType);
        this.branchName = branchName;
        this.address = address;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBranchName() {
        return branchName;
    }

    public String getAddress() {
        return address;
    }
}

class BankAccount extends BankBranch{
    private String accountNumber;
    private String accountName;


    public BankAccount(String bankName, String bankType, String branchName, String address, String accountNumber, String accountName) {
        super(bankName, bankType, branchName, address);
        this.accountNumber = accountNumber;
        this.accountName = accountName;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getAccountName() {
        return accountName;
    }
}

