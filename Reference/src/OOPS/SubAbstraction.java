package OOPS;

public class SubAbstraction {

//    public String firstName="Sai Krishna";
//    private String lastName="Pidikiti";
//    protected String middleName="Venkata Naga";

    public static String fullName =" Venkata Naga Sai Krishna Pidikiti";
    private static String fullName1 =" Venkata Naga Sai Krishna Pidikiti";
    protected static String fullName2 =" Venkata Naga Sai Krishna Pidikiti";

    /* Final Keyword: Once the variable is declared as final you cannot declare it again or assign value again */
    final int number1 = 32;
    public void transfer(){
        System.out.println("Number 1 :"+number1*2);
    }



    /* <access specifier> <return type> <method name> <(parameters)>*/
    public int sum(int a, int b){
        return a+b;
    }

    public double sum(int a, double b){
        return a+b;
    }

    public void sumNoReturn(int a, double b){
        System.out.println("Sum :"+(a+b));
    }

    private double sumDouble(double a, double b){
        double sum = a+b;
        return sum;
    }

    private int sumInteger(double a, double b){
        double sum = a+b;
        int sum1 = Integer.parseInt(String.valueOf(sum)); //Convertion from double to string then convert to Integer
        return sum1;
    }

    protected int sumIntegerProtected(double a, double b){
        double sum = a+b;
        int sum1 = Integer.parseInt(String.valueOf(sum)); //Convertion from double to string then convert to Integer
        return sum1;
    }


    public static void main(String[] args) {
        SubAbstraction subAbstraction = new SubAbstraction();
        subAbstraction.sumInteger(3,2);
    }



}
