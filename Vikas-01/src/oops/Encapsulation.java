package oops;


/*Encapsulation: Wrapping of Data and Member Functions is called Excapsulation. Example objects and classes */
public class Encapsulation {

    public static void main(String[] args) {
        Student student = new Student();
        Student student1 = new Student("sai", "10001");
        Student student2 = new Student();

        System.out.println(" Student Name : "+student.getStudentName()+ " Roll Number : "+student.getStudentName());
        System.out.println(" Student Name : "+student1.getStudentName()+ " Roll Number : "+student1.getStudentName());
        System.out.println(" Student Name : "+student2.getStudentName()+ " Roll Number : "+student2.getStudentName());

        student.setStudentName("vikas");
        student.setStudentNumber("10001");
        student2.setStudentNumber("2000");
        student2.setStudentName("Sasank");
        System.out.println("******************************************");
        System.out.println(" Student Name : "+student.getStudentName()+ " Roll Number : "+student.getStudentName());
        System.out.println(" Student Name : "+student1.getStudentName()+ " Roll Number : "+student1.getStudentName());
        System.out.println(" Student Name : "+student2.getStudentName()+ " Roll Number : "+student2.getStudentName());
    }
}

/*OUTPUT:
* Student Name : ******** RollNumber : **************
* Student Name : ********** RollNumber :************
* */