package oops;

public class IndexForOops {
    public static void main(String[] args) {
        for(int i=1; i<=4 ;i ++){ /* i will start from 1 i.e i=1, and loop will execute until i>4 i.e if i=5 loop will break */
            if(i == 1){
                System.out.println("1. Abstraction");
            }
            else if(i == 2) {
                System.out.println("2. Encapsulation");
            }
            else if(i == 3){
                System.out.println("3. Inheritance");
            }
            else {
                System.out.println("4. Polymorphisum");
            }
        }
    }
}

/*
* 1. Abstraction
* 2. Inheritance
* 3. Encapsulation
* 4. Polymorphisum*/
