package oops;

public class IndexOfOopsUsingSwitch {
    public static void main(String[] args) {
        for (int i=0;i<4;i++){
            switch (i){
                case 0:
                    System.out.println("1. Abstraction");
                    break;
                case 1:
                    System.out.println("2. Encapsulation");
                    break;
                case 2:
                    System.out.println("3. Inheritance");
                    break;
                case 3:
                    System.out.println("4. polymorphisum");
                    break;
                default:
                    break;
            }
        }
    }
}
