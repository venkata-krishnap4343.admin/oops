package oops;

public class Student{
    private String studentName;
    private String studentNumber;

    public Student(){
    }

    public Student(String studentName, String studentNumber){
        this.studentName = studentName;
        this.studentNumber = studentNumber;
    }


    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public void setStudentNumber(String studentNumber) {
        this.studentNumber = studentNumber;
    }

    public String getStudentName() {
        return studentName;
    }

    public String getStudentNumber() {
        return studentNumber;
    }
}
